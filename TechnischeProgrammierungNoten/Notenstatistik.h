#pragma once
#define WORD_LEN 120
#define ID_LEN 32

struct Notenstatistik
{
	char modulname[WORD_LEN];
	char moduleId[ID_LEN];
	char groupId[ID_LEN];
	char semesterId[ID_LEN];
	char** groupSubIds;
	char* fileName;
	int amountGroupSubIds;
	int amountPersons;
	int amountAbsent;
	int amountGrades;
	double* grades;
	double avg;
	double stdDeviation;
};
typedef struct Notenstatistik Notenstatistik_t;

enum State
{
	STATE_START, STATE_SCAN, STATE_READ_NAME, STATE_READ_ID, STATE_READ_GRADE, STATE_UNKNOWN, STATE_END
};
typedef enum State State_t;

typedef unsigned char bool; // Bool definieren
#define true 1
#define false 0

Notenstatistik_t * newNotenstatistik();
void deleteNotestatistik(Notenstatistik_t *);

void generateNotenstatistik(Notenstatistik_t * , char[]);



void printNotenstatistik(Notenstatistik_t *);



