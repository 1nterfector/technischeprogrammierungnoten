#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Notenstatistik.h"
#define NULL_CHAR '\0'

#define MODUL_NAMES_NUM 2
char* modulNames[] = { "Technische Programmierung", "Programmierprojekt" };	 // Muss angepasst werden 



// Lokale Methoden Header
bool isDigit(char);
bool isLetter(char);
bool isValidNameChar(char);
bool isValidIdChar(char);
bool isValidGradeChar(char);
bool ignore(char);
char scanInputFile(FILE*);
State_t switchState(char);
void trim(char*, int*);
bool isModuleId(char*, int);
void setId(Notenstatistik_t *, char*, int );
void setName(Notenstatistik_t *, char*, int );
void setGrade(Notenstatistik_t *, char*, int );
double gradeToDouble(char*, int);
void calcStatistics(Notenstatistik_t*);

// Gloabale Variablen
char umlaut = NULL_CHAR;
bool nextIsGroupSubIds = false;
bool nextIsSemesterId = false;


void generateNotenstatistik(Notenstatistik_t * nsp, char filePath[]) {
	FILE * file = NULL;
	char c = ' ';
	State_t state = STATE_START;
	char stringBuffer[100];
	int stringBufferIndex = 0;

	do
	{
		if (file) {
			c = scanInputFile(file);
			stringBuffer[stringBufferIndex] = c;
			stringBufferIndex++;
		}
		

		switch (state)
		{
		case STATE_START:
			fopen_s(&file, filePath, "r");
			if (file == NULL) {
				printf("Datei konnte nicht ge�ffnet werden");
				return;
			}
			nsp->fileName = filePath;
			state = STATE_SCAN;
			break;
		case STATE_SCAN:
			if (!ignore(c)) {
				state = switchState(c);
			}
			else stringBufferIndex = 0;
			break;
		case STATE_READ_ID:
			if (!isValidIdChar(c))
			{
				if (c == ',') {
					state = STATE_READ_GRADE;
				}
				else {
					state = STATE_SCAN;
					setId(nsp, stringBuffer, stringBufferIndex-1);
					stringBufferIndex = 0;
				}
				
			}
			break;
		case STATE_READ_GRADE:
			if (!isValidGradeChar(c)) {
				state = STATE_SCAN;
				setGrade(nsp, stringBuffer, stringBufferIndex-1);
				stringBufferIndex = 0;
			}
			break;
		case STATE_READ_NAME:
			if (!isValidNameChar(c)) 
			{
				state = STATE_SCAN;
				setName(nsp, stringBuffer, stringBufferIndex-1);
				stringBufferIndex = 0;
			}
			break;
		default:
			return;
			break;
		}
	} while (state != STATE_END);
	calcStatistics(nsp);
	fclose(file);
}

// Removes Spaces at the end of the String
void trim(char* chars, int* length) {
	while (chars[*length - 1] == ' ') (*length)--;
	chars[*length] = NULL_CHAR;
	(*length)++;
}

// Checks, if chars fits the moduleId format (DDLDDDD-D-LL)
bool isModuleId(char* chars, int length) {
	if (length != 13) return false;
	for (int i = 0; i < length; i++) {
		if (i < 2 && !isDigit(chars[i])) return false;	
		if (i == 2 && !isLetter(chars[i])) return false;
		if (i > 2 && i<7 && !isDigit(chars[i])) return false;
		if (i == 7 && chars[i] != '-') return false;
		if (i == 8 && !isDigit(chars[i])) return false;
		if (i == 9 && chars[i] != '-') return false;
		if (i > 9 && i < 12 && !isLetter(chars[i])) return false;
	}
	return true;
}

void setId(Notenstatistik_t * nsp, char* chars, int length) {
	trim(chars, &length);
	
	if (nextIsGroupSubIds) {
		nextIsGroupSubIds = false;
		nextIsSemesterId = true;
		int subGroupStartIndex = 0;
		for (int i = 0; i < length; i++) {
			if (chars[i] == '-') {
				nsp->groupId[i] = NULL_CHAR;
				subGroupStartIndex = i + 1;
				break;
			}
			else nsp->groupId[i] = chars[i];
		}
		
		// Anzahl subgroups rausfinden
		nsp->amountGroupSubIds = 1;
		for (int i = subGroupStartIndex; i < length; i++) {
			if (chars[i] == '-')nsp->amountGroupSubIds++;
		}
		
		nsp->groupSubIds = malloc(nsp->amountGroupSubIds * sizeof(char*));
		char buffer[ID_LEN];
		int bufferIndex = 0;
		int groupSubIdsIndex = 0;
		for (int i = subGroupStartIndex; i < length; i++) {
			if (chars[i] != '-') {
				buffer[bufferIndex] = chars[i];
				bufferIndex++;
			}
			else {
				nsp->groupSubIds[groupSubIdsIndex] = malloc(sizeof(char) * bufferIndex +1);
				for (int j = 0; j < bufferIndex; j++)
					nsp->groupSubIds[groupSubIdsIndex][j] = buffer[j];
				nsp->groupSubIds[groupSubIdsIndex][bufferIndex] = NULL_CHAR;
				groupSubIdsIndex++;
				bufferIndex = 0;
			}
		}
		nsp->groupSubIds[groupSubIdsIndex] = malloc(sizeof(char) * bufferIndex + 1);
		for (int j = 0; j < bufferIndex; j++)
			nsp->groupSubIds[groupSubIdsIndex][j] = buffer[j];
		nsp->groupSubIds[groupSubIdsIndex][bufferIndex] = NULL_CHAR;

	}else if (nextIsSemesterId) {
		nextIsSemesterId = false;
		for (int i = 0; i < length; i++)
			nsp->semesterId[i] = chars[i];
	}
	else if (isModuleId(chars, length)) {
		for (int i = 0; i < length; i++)
			nsp->moduleId[i] = chars[i];
	}
	else{
		nsp->amountPersons ++;
	}
}

void setName(Notenstatistik_t * nsp, char* chars, int length) {
	trim(chars, &length);

	char* moduleName;
	bool same;
	for (int i = 0; i < MODUL_NAMES_NUM; i++) {
		moduleName = modulNames[i];
		same = true;
		for (int j = 0; j < length; j++)
			if (chars[j] != moduleName[j]) {
				same = false;
				break;
			}
		if (same) break;
		
	}
	if (same) {
		for (int i = 0; i < length; i++)
			nsp->modulname[i] = chars[i];
		nextIsGroupSubIds = true;
	}
	else if(length == 2 && chars[0] == 'x') {
		nsp->amountAbsent ++;
	}
}

void setGrade(Notenstatistik_t * nsp, char* chars, int length) {
	trim(chars, &length);
	if (nsp->grades == NULL) {
		nsp->grades = malloc(sizeof(double));
		nsp->grades[0] = gradeToDouble(chars, length);
	} else {
		double* newGrades = malloc(sizeof(double) * (nsp->amountGrades +1));
		for (int i = 0; i < nsp->amountGrades; i++)
			newGrades[i] = nsp->grades[i];
		newGrades[nsp->amountGrades] = gradeToDouble(chars, length);
		free(nsp->grades);
		nsp->grades = newGrades;
	}
	nsp->amountGrades++;
}

double gradeToDouble(char* chars, int length) {
	double result = 0;
	for (int i = 1; i < 6; i++)
	{
		if (chars[0] == ('0' + i)) {
			result = i;
			break;
		}
	}
	for (int i = 0; i < 8; i++)
	{
		if (chars[2] == ('0' + i)) {
			result += (i/10.0);
			break;
		}
	}
	return result;
}

Notenstatistik_t * newNotenstatistik() {
	Notenstatistik_t * nsp = (Notenstatistik_t*) malloc(sizeof(Notenstatistik_t));
	
	// Initialisierung
	nsp->fileName = NULL;
	nsp->groupSubIds = NULL;
	nsp->grades = NULL;
	nsp->groupId[0] = NULL_CHAR;
	nsp->moduleId[0] = NULL_CHAR;
	nsp->semesterId[0] = NULL_CHAR;
	nsp->modulname[0] = NULL_CHAR;
	
	nsp->amountPersons = 0;
	nsp->amountGrades = 0;
	nsp->amountGroupSubIds = 0;
	nsp->amountAbsent = -1;
	nsp->avg = 0;
	nsp->stdDeviation = 0;
	return nsp;
}

void calcStatistics(Notenstatistik_t * nsp) {
	double sum = -5*nsp->amountAbsent;
	for (int i = 0; i < nsp->amountGrades; i++)
	{
		sum += nsp->grades[i];
	}
	nsp->avg = sum / (nsp->amountGrades - nsp->amountAbsent);

	double v = 0;

	int absentGrades = 0;
	if (nsp->grades) {
		for (int i = 0; i < nsp->amountGrades; i++)
		{
			if (nsp->grades[i] == 5 && absentGrades < nsp->amountAbsent) {
				absentGrades++;
				continue;
			}
			v += (nsp->avg - nsp->grades[i])*(nsp->avg - nsp->grades[i]);
		}
	}
	v /= (nsp->amountGrades - nsp->amountAbsent);
	nsp->stdDeviation = sqrt(v);
}

void printNotenstatistik(Notenstatistik_t * nsp) {
	int i;
	printf("Notenstatistik\n");
	printf("File:       %s\n", nsp->fileName);
	printf("Modulname:  %s\n", nsp->modulname);
	printf("ModulId:    %s\n", nsp->moduleId);
	printf("SemesterId: %s\n", nsp->semesterId);
	printf("GroupId:    %s\n", nsp->groupId);

	// Subgroups
	if (nsp->groupSubIds && nsp->amountGroupSubIds > 0) {
		printf("Sub Group Ids:\n");
		for (i = 0; i < nsp->amountGroupSubIds; i++)
		{
			printf("\t%s\n", nsp->groupSubIds[i]);
		}
	}

	printf("Anzahl Personen: %i\n", nsp->amountPersons);
	printf(" davon abwesend: %i\n", nsp->amountAbsent);
	
	// Statistik
	printf("\n(Noten von abwesenden Studenten werden nicht beachtet)\n");
	printf("Avg: %f\n", nsp->avg);
	printf("Std. Abweichung: %f\n", nsp->stdDeviation);

	// Notenliste
	int absentGrades = 0;
	if (nsp->grades) {
		printf("Noten:\n");
		for (i = 0; i < nsp->amountGrades; i++)
		{
			if (nsp->grades[i] == 5 && absentGrades < nsp->amountAbsent) {
				absentGrades++;
				continue;
			}
			printf(" %.1f\n", nsp->grades[i]);
		}
	}
}

void deleteNotestatistik(Notenstatistik_t * nsp) {
	if (nsp->fileName != NULL) nsp->fileName = NULL; // (nicht free, da kein malloc)
	if(nsp->grades != NULL) free(nsp->grades);
	if (nsp->groupSubIds != NULL) {
		for (int i = 0; i < nsp->amountGroupSubIds; i++)
		{
			free(nsp->groupSubIds[i]);
		}
		free(nsp->groupSubIds);
	}
	free(nsp);
}


bool isDigit(char c) {
	if (c >= '0' && c <= '9') return true;
	return false;
}
bool isLetter(char c) {
	if (c >= 'A' && c <= 'Z') return true;
	if (c >= 'a' && c <= 'z') return true;
	return false;
}
bool isValidNameChar(char c) {
	return isLetter(c) || c==' ';
}
bool isValidIdChar(char c) {
	return isDigit(c) || isLetter(c) || c=='-';
}
bool isValidGradeChar(char c) {
	return isDigit(c) || c == ',';
}


char scanInputFile(FILE* file) {
	char c;
	if (umlaut != NULL_CHAR) {
		c = umlaut;
		umlaut = NULL_CHAR;
	}else c = fgetc(file);

	// Umlaute behandeln (ANSI Encoding)
	if (c < 0) {
		switch (c)
		{
		case '�':
			c = 'U';
			umlaut = 'e';
			break;
		case '�':
			c = 'u';
			umlaut = 'e';
			break;
		case '�':
			c = 'O';
			umlaut = 'e';
			break;
		case '�':
			c = 'O';
			umlaut = 'e';
			break;
		case '�':
			c = 'A';
			umlaut = 'e';
			break;
		case '�':
			c = 'a';
			umlaut = 'e';
			break;
		case '�':
			c = 's';
			umlaut = 'z';
			break;
		default:
			break;
		}
	}
	return c;
}

State_t switchState(char cIn) {
	if (cIn == EOF) 
		return STATE_END;
	if (isLetter(cIn)) return STATE_READ_NAME;
	if (isDigit(cIn)) return STATE_READ_ID;

	return STATE_UNKNOWN;
}

bool ignore(char cIn) {
	return !(isLetter(cIn) || isDigit(cIn) || cIn == EOF);
}

